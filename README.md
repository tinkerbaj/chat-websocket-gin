# chat-websocket-gin

<a href="https://www.buymeacoffee.com/tinkerbaj"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=tinkerbaj&button_colour=5F7FFF&font_colour=ffffff&font_family=Cookie&outline_colour=000000&coffee_colour=FFDD00" /></a>
  
Example of using gorilla websocket with gin (chat with rooms)

*   Clone repo
*   Enter in console "go mod tidy" to fetch all packages project need
*   Enter "go run main.go" to start server

Websocket route will be ws://localhost:8080/chat/ws/ + any name

for example: ws://localhost:8080/chat/ws/room1
